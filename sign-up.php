<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Simple and light sign up form</title>
  <meta name="viewport" content="width=device-width, initial-scale=1"><link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <add key="webpages:Enabled" value="true" />
  
      <link rel="stylesheet" href="css/style.css">

  
</head>

<body>
<?php
require('db.php');
// If form submitted, insert values into the database.
if (isset($_REQUEST['username'])){
        // removes backslashes
	$username = stripslashes($_REQUEST['username']);
        //escapes special characters in a string
	$username = mysqli_real_escape_string($con,$username); 
	$email = stripslashes($_REQUEST['email']);
	$email = mysqli_real_escape_string($con,$email);
	$password = stripslashes($_REQUEST['password']);
	$password = mysqli_real_escape_string($con,$password);
	$trn_date = date("Y-m-d H:i:s");
        $query = "INSERT into `users` (username, password, email, trn_date)
VALUES ('$username', '".md5($password)."', '$email', '$trn_date')";
        $result = mysqli_query($con,$query);
        if($result){
            echo "<div>
<h3>You are registered successfully.</h3>
<br/>Click here to <a href='login.php'>Login</a></div>";
        }
    }else{
?>

  <div class="user">
    <header class="user__header">
        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3219/logo.svg" alt="" />
        <h1 class="user__title">Registrasi</h1>
    </header>
    
    <form name="registration" action="" method="post" class="form"  onsubmit="return checkForm(this);">
        <div class="form__group">
            <input type="text" title="Nama tidak boleh kosong" placeholder="Username" class="form__input" name="username" required pattern="\w+" />
        </div>
        
        <div class="form__group">
            <input type="email" placeholder="Email" required class="form__input" name="email" />
        </div>
        
        <div class="form__group">
            <input title="Harus Terdapat Huruf Besar dan Kecil serta Angka" 
            type="password" placeholder="Password" class="form__input" name="password" id="pwd1" required 
            pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}"/ 
            onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');
   if(this.checkValidity()) form.pwd2.pattern = RegExp.escape(this.value);" >
        </div>
        <div class="form__group">
            <input title="Password harus sama" 
            type="password" placeholder="Confirm Password" class="form__input" name="pwd2" id="pwd2" required 
            pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" 
            onchange="    this.setCustomValidity(this.validity.patternMismatch ? this.title : '');" />
        </div>
        
        <button class="btn" type="submit" name="submit">Register</button>
    </form>
</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  

    <script  src="js/index.js"></script>

    <?php } ?>

</body>

</html>
